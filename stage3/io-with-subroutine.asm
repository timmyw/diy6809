	;; IO with sub-routines
        ;;
        ;; This effectively tests the RAM, as the call/rts require a
        ;; working stack

	;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $FF, 4096

	;; Addresses from $9000 to $FFFF are ROM, so that's where
	;; code and read-only data are located.
	ORG $9000

entry
        ;; Stack at the top of RAM (just above it - but pushing onto
        ;; the stack will decrement it first)
	lds     #$8000

        lda     $55
        sta     $8000           ; First output to IOR0

        lda     $55
        sta     $1000           ; RAM
        jsr     delay

        lda     $AA
        sta     $8800           ; Second output - to IOR4

        lda     $55
        sta     $1000           ; Save to RAM (RAMEN goes low)

        jsr     delay

        jmp     entry

delay
	lda #0
1
	jsr     delay_inner
	deca
	cmpa    #$ff
	blo     1B
	rts

;; Inner delay subroutine: clobbers X.
delay_inner
	ldx #0
1
	leax    1, x
	cmpx    #$0fff
	blo     1B
	rts

	;; reset vector
	ORG $FFFE
	FDB entry
