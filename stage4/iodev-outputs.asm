	;; IO with sub-routines
        ;;
        ;; This effectively tests the RAM, as the call/rts require a
        ;; working stack

        include "../lib/io.asm"

	;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $FF, 4096

	;; Addresses from $9000 to $FFFF are ROM, so that's where
	;; code and read-only data are located.
	ORG $9000

entry
        ;; Stack at the top of RAM
	lds     #$7ffe

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV00) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV01) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV02) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV03) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV04) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV05) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV06) ; First output to IOR0
        jsr     delay

        lda     #$55
        sta     (IOBASE+IOREG00+IODEV07) ; First output to IOR0
        jsr     delay

        jmp     entry

delay
	lda #0
1
	jsr     delay_inner
	deca
	cmpa    #$7f
	blo     1B
	rts

;; Inner delay subroutine: clobbers X.
delay_inner
	ldx #0
1
	leax    1, x
	cmpx    #$0fff
	blo     1B
	rts

	;; reset vector
	ORG $FFFE
	FDB entry
