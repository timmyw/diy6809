;; Loop test
	ORG $8000
	FILL $FF, 4096

	ORG $9000

main
	lda #5
	adda #10

last
	jmp main

	;; reset vector
	ORG $FFFE
	FDB main

;; vim:ft=asm6809:
