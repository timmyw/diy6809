;; Input/output tests

OPORT EQU $8000

	;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $ff, 4096								; Fill with $ff

	;; entry point for input/output test program
entry

	;;lda #$55
	lda $9000											; Force an RMEM
	;;sta OPORT											; Force a WMEM, IODEV0 and IOR0
  nop
  nop
  nop
  nop
  nop
	jmp entry											; Rinse and repeat

	;; Reset vector: jump to entry point
	ORG $FFFE
	FDB entry
