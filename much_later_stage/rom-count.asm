;; Count program which uses only ROM

OPORT EQU $8000

;; Bits controlling LED display segments.
SEG_A EQU (1<<0)
SEG_B EQU (1<<1)
SEG_C EQU (1<<2)
SEG_D EQU (1<<3)
SEG_E EQU (1<<4)
SEG_F EQU (1<<5)
SEG_G EQU (1<<6)

display1 macro
	ldx #hexfont               ; Load base address of hex font
	ldb a,x                    ; Load bit pattern for hex digit
	stb OPORT
	endm

;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $FF, 4096

	ORG $9000

main

	lda #5

	ldb #9
	adda #10
inner
	;; ldx #hexfont               ; Load base address of hex font
	;; ldb a,x                    ; Load bit pattern for hex digit
	;; stb OPORT

last	
	jmp last
	
	ldx #0
1
	leax 1, x
	cmpx #$7fff
	blo 1B
	rts

	ldx #0
1
	leax 1, x
	cmpx #$7fff
	blo 1B

	ldx #0
2
	leax 1, x
	cmpx #$7fff
	blo 2B

  inca
  anda #$0f
  jmp inner

hexfont
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F)
	FCB ~(SEG_B|SEG_C)
	FCB ~(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G)
	FCB ~(SEG_B|SEG_C|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_B|SEG_C|SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_A|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_E|SEG_F|SEG_G)
	
	
	;; reset vector
	ORG $FFFE
	FDB main

;; vim:ft=asm6809:
