	;;  Count test for ROM+RAM
	;;

display1 macro
	ldx #hexfont               ; Load base address of hex font
	ldb a,x                    ; Load bit pattern for hex digit
	stb OPORT
	endm
	
;; There is a 74HCT574 mapped at $8000
OPORT EQU $8000

;; Bits controlling LED display segments.
SEG_A EQU (1<<0)
SEG_B EQU (1<<1)
SEG_C EQU (1<<2)
SEG_D EQU (1<<3)
SEG_E EQU (1<<4)
SEG_F EQU (1<<5)
SEG_G EQU (1<<6)
	
;; Count variable in RAM
count_var EQU $1000

	;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $FF, 4096

	;; Addresses from $9000 to $FFFF are ROM, so that's where
	;; code and read-only data are located.
	ORG $9000
	
count_entry
	;; put system stack at top of RAM
	lds #$7FFE

	;; clear count variable
	lda #0
	sta count_var

main_loop
	lda count_var                 ; load count variable

	;; display1
	jsr display

	inca                          ; increment count
	anda #$0f
	sta count_var                 ; store updated value to count

	jsr delay                     ; delay
	jmp main_loop                 ; repeat main loop

;;; Display routine.  Clobbers X and B
display
	ldx #hexfont               ; Load base address of hex font
	ldb a,x                    ; Load bit pattern for hex digit
	stb OPORT
	rts
	
;; delay routine: clobbers A and X.
delay
	lda #0
1
	jsr delay_inner
	deca
	cmpa #$ff
	blo 1B
	rts

;; Inner delay subroutine: clobbers X.
delay_inner
	ldx #0
1
	leax 1, x
	cmpx #$7fff
	blo 1B
	rts

hexfont
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F)
	FCB ~(SEG_B|SEG_C)
	FCB ~(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G)
	FCB ~(SEG_B|SEG_C|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_B|SEG_C|SEG_D|SEG_E|SEG_G)
	FCB ~(SEG_A|SEG_D|SEG_E|SEG_F|SEG_G)
	FCB ~(SEG_A|SEG_E|SEG_F|SEG_G)
	
	
	;; reset vector
	ORG $FFFE
	FDB count_entry

