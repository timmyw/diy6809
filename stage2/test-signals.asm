	;;  Force generation of signals
	;;  WMEM and IOR*

	;; Addresses from $8000 to $8FFF are reserved for I/O devices.
	ORG $8000
	FILL $FF, 4096

	;; Addresses from $9000 to $FFFF are ROM, so that's where
	;; code and read-only data are located.
	ORG $9000

loop
	lda $55
	sta $1234               ; Would be RAM but should result in a
                                ; WMEM signal

        lda $aa
        sta $8000               ; IO will result in a IOR0 signal

        lda $55
        sta $8800               ; IOR4 signal

	jmp loop

	;; reset vector
	ORG $FFFE
	FDB loop
