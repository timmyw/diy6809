# Assembler
ASM=asm6809
ASMFLAGS=--verbose --6809 --bin

# Programmer
PROGRAMMER=minipro
EEPROM=AT28C256
PROGFLAGS=-p ${EEPROM} -w

%.bin : %.asm
	${ASM} ${ASMFLAGS} -l $*.lst -B $< --output $*.bin

program:
	${PROGRAMMER} ${PROGFLAGS} ${BINFILE}

clean:
	rm -f *.bin *.lst *.hex
