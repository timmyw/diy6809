
;;; General constants defining areas of the IO memory

;;; Start of IO memory (we have 4096 bytes of IO - divided into 8
;;; regions, each of which can be further broken down into 16 devices,
;;; each with 32 bytes.
IOBASE          EQU     $8000

;;; Beginning of IOREG00
IOREG00         EQU     $0000

;;; Device offsets
IODEV00         EQU     $0000
IODEV01         EQU     $0020
IODEV02         EQU     $0040
IODEV03         EQU     $0060
IODEV04         EQU     $0080
IODEV05         EQU     $00a0
IODEV06         EQU     $00c0
IODEV07         EQU     $00e0
IODEV08         EQU     $0100
IODEV09         EQU     $0120
IODEV10         EQU     $0140
IODEV11         EQU     $0160
IODEV12         EQU     $0180
IODEV13         EQU     $01a0
IODEV14         EQU     $01c0
IODEV15         EQU     $01e0
